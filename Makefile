all: paper.pdf paper.aux

%.aux: %.tex
	pdflatex ${basename $<}

%.bbl: %.aux
	bibtex ${basename $<}

%.pdf: %.bbl
	pdflatex ${basename $<}
	pdflatex ${basename $<}

paper.pdf: paper.tex \
	dropping.pdf \
	pww-spark.pdf \
	sliding-window.pdf \
	window-widening.pdf \
	refs.bib


clean:
	rm -f *.aux *.log *.bbl *.blg 

distclean: clean
	rm -f paper.pdf
